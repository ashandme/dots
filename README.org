#+title: My Dots
#+author: ashandme

* My setup
** Emacs
   it comes with some packages that i use (im not using MELPA in this config)
** Sway
   it has a minimal config and some easy keybinds.
   - Mod + c *execute terminal*
   - Mod + e *execute editor*
   - Mod + w *execute brownser*
   - Mod + p *execute pdf viewer*
   - Mod + f7 *lock system*
   - Mod + x *kill application*
   - Mod + r *reload sway*
** BSPWM
   I am currently using this. It has a configuration that is the one that comes by default
*** sxhkd
    keybinds are designed for halmak layout.
*** yabar
    shows only the important things like battery, memory usage, cpu usage, temperature, and date.
** Other programs
   i also used kakoune, you can see my config in ./config/kak/
   MPV is a nice media player that i use as a video player (./config/mpv/)and image viewer (./config/mvi/)
