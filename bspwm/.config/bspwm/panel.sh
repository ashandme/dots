#!/bin/sh


BAR=/tmp/pipe_info_bar

[ -e "$BAR" ] && rm "$BAR"
mkfifo "$BAR"

# Define the clock
Battery() {
    BATTERY=$(cat /sys/class/power_supply/BAT0/capacity)
    printf "$BATTERY"
}

Mem() {
    MEMORY=$(free -h | grep 'Mem' | tr -s ' ' | cut -f 3 -d ' ' | sed 's/i//g')
    printf "$MEMORY"
}

Clock() {
    DATETIME=$(date "+%a %b %d, %T")
    printf "$DATETIME"
}

# Print the clock

while true; do
        echo "$(Mem) %{c}%{F#FFFF55}%{B#5555FF} -=|[$(Clock)]|=- %{F-}%{B-} %{r} $(Battery)" > $BAR
        sleep 1
done
