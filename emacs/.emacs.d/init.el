(require 'package)
;; Add the NonGNU elpa package archive
;; Sad but we need this
(add-to-list 'package-archives  '("nongnu" . "https://elpa.nongnu.org/nongnu/"))

(menu-bar-mode -1)

(tool-bar-mode -1)

(scroll-bar-mode -1)

;; >> ADDING PACKAGES <<
(package-initialize)

(defun pkg-add (a &optional)
  (unless (package-installed-p a)
    (package-refresh-contents)
    (package-install a)))

;; Dash package
(pkg-add 'dash)

;; Completion framework
(pkg-add 'vertico)

;; Enable completion by narrowing
(vertico-mode t)

;; Zenburn theme
(pkg-add 'zenburn-theme)

;; Set emacs theme
(load-theme 'zenburn t)

;; View VCS changes in file
(pkg-add 'diff-hl)

;; Enable `diff-hl' support by default in programming buffers
(add-hook 'prog-mode-hook #'diff-hl-mode)

;; Rust Support
(pkg-add 'rust-mode)

;; FlyCheck for syntax errors
(pkg-add 'flymake)

;; Company mode
(pkg-add 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; Eglot
(pkg-add 'eglot)

;; Raku Support
(pkg-add 'raku-mode)

;; Miscellaneous options
(setq-default major-mode
              (lambda () ; guess major mode from file name
                (unless buffer-file-name
                  (let ((buffer-file-name (buffer-name)))
                    (set-auto-mode)))))
(save-place-mode t)
(savehist-mode t)
(recentf-mode t)
(set-face-attribute 'default nil :height 140)
(setq backup-directory-alist `(("." . "~/.saves")))
;; Store automatic customisation options elsewhere
(setq custom-file (locate-user-emacs-file "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))
