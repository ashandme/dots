#!/bin/sh
print_date() {
    echo "<span foreground=\"#ffffff\">$(date +'%a %d %H:%M')</span>"
}
print_mem() {
    echo "MEM: <span foreground=\"#00ffff\">$(free -h | grep 'Mem' | tr -s ' ' | cut -f 3 -d ' ' | sed 's/i//g')</span>"
}
print_temp() {
    celsious="$(sensors | grep temp1 | cut -f 9 -d " " | sed "s/+//g;s/°C//g;1q")"
    echo "TEM: <span foreground=\"#ff00ff\">${celsious}C</span>"
}
print_battery() {
	perc="$(cat "$1/capacity")"
	printf "BAT: "
	if [ "$(cat "$1/status")" = Charging ]; then
		echo "<span foreground=\"#00ff00\">$perc%</span>"
	elif [ "$perc" -lt 15 ]; then
		echo "<span foreground=\"#ff0000\">$perc%</span>"
    	notify-send --ungency=critical "Low Battery (${perc})" "need caffeine"
	else
		echo "<span foreground=\"#ffff00\">$perc%</span>"
	fi
}
print_vol() {
    echo "VOL: <span foreground=\"#ff00ff\">$(pulsemixer --get-volume | cut -d' ' -f 1)</span>"
}
print_backlight() {
	echo "BKL: <span foreground=\"#0000ff\">$(light -G)%</span>"
}
print_all() {
	echo \
        "[$(print_backlight)] " \
		"[$(print_vol)] " \
		"[$(print_battery /sys/class/power_supply/BAT0)] " \
		"[$(print_temp)] " \
		"[$(print_mem)] " \
		"[$(print_date)] "
	return $?
}
while print_all; do sleep 4 & wait $!; done
